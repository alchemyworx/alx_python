![Alchemy Worx][logo]
[logo]: https://www.alchemyworx.com/public/sites/default/files/alchemy_worx_logo.jpg
# Alchemy Worx Python Library

This library is a public data-manipulation toolset, indended to ease the process of extracting meaningful conclusions from data.

Most functions defined in this library are extensions to the [Pandas][pd] library. The rest: [datetime][dt] and [matplotlib][mpl].
[pd]: https://pypi.org/project/pandas/
[dt]: https://docs.python.org/3/library/datetime.html
[mpl]: https://pypi.org/project/matplotlib/

## Installation and Use:

Install using the following command:

`pip install git+https://git@bitbucket.org/alchemyworx/alx_python.git@master`

Access with the following:

```python
import alx

print(alx.this_week)

>> DatetimeIndex(['2019-10-13', '2019-10-14', '2019-10-15', 
                  '2019-10-16', '2019-10-17', '2019-10-18', 
                  '2019-10-19'], dtype='datetime64[ns]', freq='D')
```

## Documentation

#### Date Reference

All reporting dates are respective of the most recently completed week, where Sunday begins every week.

`alx.Dates()`
: class, parent to the following variables.
    Params:
    date: str, date, datetime; the referential date to base the following values.

    All of the below default to valeus with reference to the datetime of code execution.
    Provide a custom date and refer to each of the below verbosely to customize date-reference.
    Example:
        alx.Dates(datetime.datetime(2011, 10, 11)).this_week

`alx.sun`
: datetime.datetime, The most recently completed week's Sunday.

`alx.sat`
: datetime.datetime, The most recently completed week's Saturday.

`alx.sun_ly`
: datetime.datetime, The most recently completed week's equivalent from last year's Sunday.

`alx.sat_ly`
: datetime.datetime, The most recently completed week's equivalent from last year's Saturday.

`alx.this_week`
: pd.date_range, The most recently completed week.

`alx.last_week`
: pd.date_range, The week prior to the most recently completed week.

`alx.this_week_ly`
: pd.date_range, The most recently completed week's equivalent from last year.

`alx.prev_6w`
: pd.date_range, The 6 weeks prior to the most recently completed week.

`alx.prev_6w_ly`
: pd.date_range, The 6 weeks prior to the most recently completed week's equivalent from last year.

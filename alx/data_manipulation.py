import alx.constants as const
import calendar
import datetime as dt
import numpy as np
import pandas as pd

from dateutil.parser import parse
from enum import Enum
from typing import List, Union

__all__ = [
    'index_by_avail',
    'date_filter',
    'ratify',
    'fillna_body',
    'pivot',
    'pct_diff',
    'window',
    'form',
    'comp',
    'TableType'
]


def index_by_avail(df, columns, fill_missing: bool = False, fill_value: Union[None, type(np.nan), dict] = np.nan):
    if fill_missing:
        for col in [c for c in columns if c not in df.columns]:
            df[col] = fill_value if not isinstance(fill_value, dict) else fill_value.get(col)
    return df[[col for col in columns if col in df.columns]]


def date_filter(
    df: pd.DataFrame,
    *args,
    start: Union[dt.date, dt.datetime] = None,
    end: Union[dt.date, dt.datetime] = None,
    year: int = None,
    weeks: List[int] = None,
    week: int = None,
    date_range: pd.date_range = None
) -> pd.DataFrame:
    """Trim a dataframe for a specified date-range, provided it has a DateTimeIndex.

    Dates and ranges are parsed as best as possible, in whatever format they come.
    By default, `alx.this_week` is provided.

    Arguments:
        df {pd.DataFrame} -- dataframe with a DateTimeIndex

    Keyword Arguments:
        start {dt.datetime, str} -- start date (default: {None})
        end {dt.datetime, str} -- end date (default: {None})
        year {int} -- year, must include week/weeks (default: {None})
        weeks {List[int]} -- weeks of interest, must include year (default: {None})
        week {int} -- week of interest, must include year (default: {None})
        date_range {pd.date_range} -- complete date range of interest (default: {None})

    Raises:
        ValueError: Too many arguments / Wrong Format

    Returns:
        pd.DataFrame -- trimmed dataframe
    """
    if all(argument is None for argument in [start, end, year, weeks, week, date_range]):
        if not args:
            start = const.sun
            end = const.sat
        else:
            if len(args) > 2:
                raise ValueError('Too many arguments given. Provide [(date_range), (start,end), (year,week/s)]')
            if len(args) == 1:
                assert isinstance(args[0], pd.DatetimeIndex), ValueError('If providing one argument, pass it through as pd.DatetimeIndex.')
                start = args[0].min()
                end = args[0].max()
            else:
                if any([
                        all([isinstance(x, str) for x in args]),
                        all([isinstance(x, dt.date) for x in args]),
                        all([isinstance(x, dt.datetime) for x in args]),
                ]):
                    start = args[0]
                    end = args[1]
                else:
                    year = [x for x in args if x > 1900][0]
                    weeks = [x for x in args if x != year]
    if all([(x is not None) for x in [start, end]]):
        start = parse(start) if isinstance(start, str) else start
        end = parse(end) if isinstance(end, str) else end
        if start > end:
            start, end = end, start
        start = pd.to_datetime(start)
        end = pd.to_datetime(end)
    elif all([year, any([week, weeks])]):
        assert not all([week, weeks]), ValueError('Please provide only one week descriptor.')
        weeks = [week] if week else weeks
        assert isinstance(weeks, list), TypeError('weeks must be a list.')
        start = const.date_from_parts(year, min(weeks), 0)
        end = const.date_from_parts(year, max(weeks), 6)
        start = pd.to_datetime(start)
        end = pd.to_datetime(end)
    elif date_range is not None:
        start = date_range.min()
        end = date_range.max()
    else:
        raise ValueError('Provide clearer date-range information.')
    if type(start) == type(end) == df.index.dtype:
        pass
    elif df.index.dtype == np.dtype('datetime64[ns]'):
        start = dt.datetime(int(start.year), int(start.month), int(start.day))
        end = dt.datetime(int(end.year), int(end.month), int(end.day))
    else:
        raise ValueError('Index column does not appear to be in the correct format.')
    return df[(df.index >= start) & (df.index <= end)]


def ratify(df: pd.DataFrame, heads: const.Heads = const.heads) -> pd.DataFrame:
    """Calculate engagement and conversion rates, provided standard aggregate rates.

    This function expects column headers to follow commonplace standards which are specified by `alx.headers`.
    Rates which are unable to be calculated due to absent dependents will not be included as nan.

    Arguments:
        df {pd.DataFrame} -- dataframe with conventional ALX headers

    Returns:
        pd.DataFrame -- dataframe with rates columns included
    """

    header_height = len(df.columns.names) - 1
    for _ in range(header_height):
        df = df.stack(dropna=False)
    for header in heads.subset('rate'):
        try:
            col_a, col_b = header.derived_columns
            df[header.name] = header.multiplier * (df[col_a].astype(float) / df[col_b].astype(float))
        except (TypeError, IndexError, ValueError, KeyError):
            # Anticipating issues in:
            # - Dividing strings
            # - Seeking nonexistent rows
            # - Converting strings to floats
            pass
    for _ in range(header_height):
        df = df.unstack()
    return df


def fillna_body(df: pd.DataFrame, unstacked: bool = False) -> pd.DataFrame:
    """Fill continuum holes

    DFs which contain NaN holes in spaces which are meant to be 0 ought to be filled.
    This applies to historical data which is assured to be (NULL < val < 1)

    Arguments:
        df {pd.DataFrame} -- dataframe with conventional ALX headers

    Keyword Arguments:
        unstacked {bool} -- flag as to whether or not there are hierarchical column headers to deal with (default: {False})

    Returns:
        pd.DataFrame -- dataframe with conventional ALX headers
    """

    if unstacked:
        df_res = pd.DataFrame()
        for col in df.columns.get_level_values(0).unique():
            df_slice = df[col].pipe(fillna_body)
            df_slice.columns = pd.MultiIndex.from_product(
                [[col], df_slice.columns],
                names=['Metric', 'Year']
            )
            df_res = pd.concat([df_res, df_slice], axis=1)
        return df_res
    df[~(df.bfill().isnull() | df.ffill().isnull())].fillna(0, inplace=True)

    # Week 53 is likely not skipped by data omission. Undo.
    if '53' in df.index:
        try:
            df.loc['53'].replace(0, np.nan, inplace=True)
        except KeyError:
            pass
    if 53 in df.index:
        try:
            df.loc[53].replace(0, np.nan, inplace=True)
        except KeyError:
            pass

    return df


class ByOption(Enum):
    week: str = 'week'
    month: str = 'month'
    quarter: str = 'quarter'


def pivot(
    df: pd.DataFrame,
    *,
    freq: str = None,
    by: str = None,
    month_name: bool = True,
    short_months: bool = True,
    heads: const.Heads = const.Heads(),
    years_back: int = -1
) -> pd.DataFrame:
    if freq and by:
        raise ValueError('Only supply freq or by.')
    elif not (freq or by):
        by = 'week'
    if freq:
        pass
    elif by.lower() == 'week':
        freq = 'W-SAT'
    elif by.lower() == 'month':
        freq = 'M'
    elif by.lower() == 'quarter':
        freq = 'Q'
    else:
        raise ValueError('Must supply freq or by.')
    if years_back < 0:
        years_back = df.index.year.max() - df.index.year.min()

    index_name = {
        'B': 'Day',
        'C': 'Day',
        'D': 'Day',
        'W': 'Week',
        'M': 'Month',
        'Q': 'Quarter'
    }.get(freq.upper()[0], 'Week')

    df_pivot = (
        pd.merge(
            (
                df
                .resample(freq, kind='period').sum()
            ),
            (
                const.weekly_comp_table_by_year(
                    df.index.max().year - years_back,
                    df.index.max().year,
                    freq=freq,
                    square=True,
                    bump_index=True,
                )
                .stack('Year')
                .to_frame('Date')
                .reset_index()
            ),
            on='Date',
            how='right'
        )
        .pivot(
            index=index_name,
            columns='Year'
        )
    )
    df_pivot.index = (
        pd.Series(df_pivot.index)
        .apply(lambda x: str(x).zfill(df_pivot.index.astype(str).str.len().max()))
    )
    df_pivot = df_pivot.pipe(ratify, heads=heads)
    if freq.upper().startswith('M'):
        if short_months:
            months = calendar.month_abbr
        else:
            months = calendar.month_name
        df_pivot.index = df_pivot.index.to_series().astype(int).apply(lambda x: months[x])

    return df_pivot


def pct_diff(df: pd.DataFrame, index: str = '% Diff', append: bool = True, inf_to_zero: bool = True) -> pd.DataFrame:
    """Calculate the %-difference between the bottom two rows in any df.
    Typically used after some data manipulation for comparison.

    Arguments:
        df {pd.DataFrame} -- dataframe with conventional ALX headers

    Keyword Arguments:
        index {str} -- Custom index label for the row containing difference %s (default: {'% Diff'})
        append {bool} -- whether to append the % diff row to the bottom of the provided table (default: {True})
        inf_to_zero {bool} -- whether to replace infinity values with 0 (default: {True})

    Returns:
        pd.DataFrame -- dataframe outlining % difference between last two lines
    """

    index = pd.Index([index]) if isinstance(index, str) else index
    df_index_width = len(df.index.names)
    # Check to see if current index and planned index are the same size.
    # If this is the case, and lines are to be appended, fix it.
    if (append & (df_index_width != len(index.names))):
        index = pd.MultiIndex(
            levels=[['']] * (df_index_width - 1) + [index],
            codes=[[0]] * df_index_width,
            # TODO: Address why these names are not being passed through.
            names=df.index.names
        )
    res = df.pct_change().iloc[-1].to_frame().T
    if inf_to_zero:
        res.replace((np.inf, -np.inf), 0, inplace=True)
    res.fillna(0, inplace=True)
    res.index = index
    if append:
        res = pd.concat([df, res])
    return res


def window(
    df: pd.DataFrame,
    week_radius: int = 6,
    years_back: int = 1,
    *,
    date: Union[str, dt.datetime, dt.date, pd.Timestamp] = None,
    week_end: str = 'Saturday',
    freq: str = 'W-SAT',
    heads: const.Heads = const.heads,
    dates: const.Dates = None
) -> pd.DataFrame:

    if dates is None:
        dates = const.Dates(date, week_end=week_end)

    df_week_block = const.weekly_comp_table_by_year(
        dates.sat.year - (years_back + 1),
        dates.sat.year + 1,
        freq=freq,
        bump_index=True,
        index_name='Week',
        square=True
    )

    df_week_block = pd.concat([
        df_week_block.shift(1, axis=1),
        df_week_block,
        df_week_block.shift(-1, axis=1),
    ])
    df_week_block = df_week_block.iloc[:, 1:-1].drop_duplicates(keep='last')

    df_week_block.index = pd.Series(df_week_block.index).apply(lambda x: str(x).zfill(2))
    df_week_block = df_week_block.reset_index()

    row_of_interest = df_week_block.loc[df_week_block[dates.sat.year] == pd.Period(dates.this_week.max(), freq=freq)].index

    df_week_block = df_week_block.iloc[int(row_of_interest.max() - week_radius): int(row_of_interest.max() + week_radius + 1), [0] + list(range(2, len(df_week_block.columns)))]

    df_window = pd.merge(
        (
            df
            .resample(freq, kind='period').sum()
        ),
        (
            df_week_block
            .set_index('Week')
            .stack('Year')
            .to_frame('Date')
            .reset_index()
        ),
        on='Date',
        how='right'
    )

    df_window = df_window.pivot(
        index='Week',
        columns='Year'
    ).reindex(df_week_block['Week'])

    df_window = (
        df_window
        .pipe(ratify, heads=heads)
        .sort_values([('Date', df_window.columns.get_level_values('Year').max())])
    )

    return df_window


def form(
    df: pd.DataFrame,
    row_spec: dict = {},
    col_spec: dict = {},
    default: 'str' = 'number',
    auto_pct: bool = True,
    heads: const.Heads = const.heads
) -> pd.DataFrame:
    """Provide standard formatting to a given df.

    Unless specifically dictated by user, default function formats data in a column-wise fashion
    by seeking out conventional ALX headers and their expected format defined in this library.
    Formatting is attempted wherever possible, and returns the original value where formatting fails.

    Arguments:
        df {pd.DataFrame} -- dataframe with conventional ALX headers

    Keyword Arguments:
        row_spec {dict} -- specific row-wise instructions by integer (default: {{}})
            Specify instructions for formatting provided by
            `alx.format_dict` or custom format strings as follows:
                row_spec = {
                    'percent': [0, 2, 3],
                    '€{:,.0f}': [1]
                }
        col_spec {dict} -- specific column-wise instructions by name (default: {{}})
            Specifications are made in opposite manner as found above:
                col_spec = {
                    'Delivered': 'percent',
                    'Other Col': '€{:,.0f}'
                }
        default {const.Format} -- the default format to apply on columns which are not dictated in form by ALX (default: {'number'})
        auto_pct {bool} -- whether to specifically seek out rows which contain 'diff' in the index name and format as percents (default: {True})

    Raises:
        ValueError: Formatting string is incorrect / Default format is not appropriate.

    Returns:
        pd.DataFrame -- dataframe with dictated formatting
    """

    header_height = len(df.columns.names) - 1
    for _ in range(header_height):
        df = df.stack()
    df = df.fillna(0)
    res = df.copy()
    for col in df.columns:
        if col in col_spec:
            try:
                form = const.column_format.get(col_spec[col], col_spec[col])
            except KeyError:
                raise ValueError('Format string is in the wrong format.')
        else:
            options = [
                field for field in vars(heads).values()
                if col in [field.name, field.short_name, field.sql_name]
            ]
            if len(options) == 0:
                if any([term.lower() in ['cost', 'revenue', 'rev', 'dollar'] for term in col.split()]):
                    form = const.format_dict['dollar']
                else:
                    form = const.format_dict[default]
            else:
                form = options[0].formatting
        if form is None:
            continue
        try:
            res[col] = df[col].apply(form.format)
        except ValueError:
            pass
    if row_spec:
        for k, v in row_spec.items():
            form = k if all(x in k for x in ['{', '}']) else const.format_dict[k]
            if form is None:
                continue
            v = [v] if not isinstance(v, list) else v
            for i in v:
                res.iloc[i] = df.iloc[i].apply(form.format)
    for _ in range(header_height):
        res = res.unstack()
    if auto_pct:
        # Check the right-most index column for the term 'Diff'. Apply to all.
        r_most_index = df.index.get_level_values(len(df.index.names) - 1)
        if r_most_index.dtype == 'object':
            try:
                pct_diff_indices = r_most_index.get_loc('% Diff')
            except KeyError:
                # Searched Index value is not available.
                return res
            if isinstance(pct_diff_indices, int):
                pct_diff_indices = [(x == pct_diff_indices) for x in range(len(res))]
            for i, diff_row in enumerate(pct_diff_indices):
                if not diff_row:
                    continue
                res.iloc[i] = df.iloc[i].apply(const.column_format['percent'].format)
    return res


class TableType(Enum):
    ytd: str = 'ytd'
    mtd: str = 'mtd'
    wow: str = 'wow'
    yoy: str = 'yoy'


def comp(
    df: pd.DataFrame,
    kind: TableType = 'wow',
    params: dict = {},
    date: Union[dt.date, dt.datetime] = None,
    incl_pct_diff: bool = True,
    avg_row: Union[str, bool] = 'default',
    week_end: str = 'Saturday',
    heads: const.Heads = const.heads,
    average_length: int = 6,
) -> pd.DataFrame:
    """Creates Comparison Data Table.
    YTD and MTD comparisons default to complete periods when the option is present.

    Arguments:
        df {pd.DataFrame} -- dataframe with conventional ALX headers

    Keyword Arguments:
        kind {TableType, str} -- default parameter set for report type: ['ytd','mtd','wow','yoy'] (default: {'wow'})
        params {dict} -- custom parameters to apply; if kind is chosen, these overwrite default values (default: {{}})
            Formatting for param design is as follows:
                params = {
                    'freq': 'Y',
                    'filter': (year_condition & (month_condition | (latest_month_condition & date_condition))),
                    'title': 'YTD',
                    'pct_diff': bool,
                    'rows':[
                        {
                            'name': 2018,
                            'range': 2018,
                            'agg':'sum'
                        },
                        {
                            'name': 2019,
                            'range': 2019,
                            'agg': ['sum', 'mean']
                        }
                    ]
                }
        date {dt.datetime} -- focus date for the comparison; analysis is done on a weekly basis by default (default: {None})
            Absent choice defaults to the most recent completed week (Last Sun-Sat)
        incl_pct_diff {bool} -- whether to include a percent-difference row at the bottom of the table (default: {True})
        avg_row {bool} -- whether to include a 6W average row in the middle of the table (default: {'default'})
            True for 'wow' tables. False for 'yoy'. Inapplicable to others.

    Raises:
        ValueError: default parameter choice is invalid

    Returns:
        pd.DataFrame -- dataframe providing detailed aggregative comparison
    """
    # TODO: Extract the `params` object as a separate entity.

    date = date or dt.date.today()
    week_freq = f'W-{week_end[:3].upper()}'
    kind = (
        kind.lower().replace('/', 'o').replace(' ', '') if isinstance(kind, str) else ''
    )
    if isinstance(avg_row, bool):
        pass
    elif avg_row == 'default':
        if hasattr(TableType, kind):
            table_type = TableType(kind)
            if table_type == TableType.wow:
                avg_row = True
            elif table_type == TableType.yoy:
                avg_row = False
            else:
                avg_row = None
        else:
            avg_row = None
    else:
        raise ValueError('avg_row should be a boolean value or "default".')

    if hasattr(TableType, kind):
        table_type = TableType(kind)
        d = const.Dates(date, week_end=week_end)
        year = d.this_week.to_period('Y').min().year  # get min year in focus week to default full-year analysis.
        month = d.this_week.to_period('M').min().month  # get min month in focus week to default full-month analysis.
        day = d.this_week.to_period('D').day.max()  # get largest day-value in focus week to ensure complete term analysis, if straddling terms.

        if table_type == TableType.ytd:
            # This definition stutters at the end of each month.
            # At the turn of each month, this YTD value sticks to the last day of the most recently ended month, even if it was mid-week.
            primary_range = d.this_week.to_period('Y').min()
            year_condition = df.index.year.isin([year - 1, year])
            month_condition = (df.index.month < month)
            latest_month_condition = (df.index.month == month)
            date_condition = (df.index.day <= day)
            default_params = {
                'freq': 'Y',
                'filter': (year_condition & (month_condition | (latest_month_condition & date_condition))),
                'title': 'YTD',
                'pct_diff': incl_pct_diff,
                'rows': [
                    {
                        'name': primary_range - 1,
                        'range': primary_range - 1,
                        'agg': 'sum'
                    },
                    {
                        'name': primary_range,
                        'range': primary_range,
                        'agg': 'sum'
                    }
                ]
            }
        elif table_type == TableType.mtd:
            primary_range = d.this_week.to_period('M').min()
            year_condition = df.index.year.isin([year - 1, year])
            month_condition = (df.index.month == month)
            date_condition = (df.index.day <= day)
            default_params = {
                'freq': 'M',
                'title': 'MTD',
                'filter': (year_condition & month_condition & date_condition),
                'pct_diff': incl_pct_diff,
                'rows': [
                    {
                        'name': (primary_range - 12).year,
                        'range': primary_range - 12,
                        'agg': 'sum'
                    },
                    {
                        'name': primary_range.year,
                        'range': primary_range,
                        'agg': 'sum'
                    }
                ]
            }
        elif table_type == TableType.wow:
            week_condition = df.index.to_period('D').start_time.isin(d.this_week.append(d.prev_Nw(average_length)))
            last_week_sun = d.last_week.min().strftime('%-m/%-d')
            last_week_sat = d.last_week.max().strftime('%-m/%-d')
            this_week_sun = d.this_week.min().strftime('%-m/%-d')
            this_week_sat = d.this_week.max().strftime('%-m/%-d')
            default_params = {
                'freq': week_freq,
                'title': 'W / W',
                'filter': week_condition,
                'pct_diff': incl_pct_diff,
                'rows': [
                    {
                        'name': '-'.join([last_week_sun, last_week_sat]),
                        'range': d.last_week.to_period(week_freq).min(),
                        'agg': 'sum'
                    },
                    {
                        'name': '6W Mean',
                        'range': d.prev_Nw(average_length).to_period(week_freq).unique(),
                        'agg': ['sum', 'mean']
                    },
                    {
                        'name': '-'.join([this_week_sun, this_week_sat]),
                        'range': d.this_week.to_period(week_freq).min(),
                        'agg': 'sum'
                    }
                ]
            }
        elif table_type == TableType.yoy:
            week_condition = df.index.to_period('D').start_time.isin(d.this_week.append([d.this_week_ly, d.prev_Nw_ly(average_length)]))
            this_week_ly_sun = d.this_week_ly.min().strftime('%-m/%-d/%y')
            this_week_ly_sat = d.this_week_ly.max().strftime('%-m/%-d/%y')
            this_week_sun = d.this_week.min().strftime('%-m/%-d/%y')
            this_week_sat = d.this_week.max().strftime('%-m/%-d/%y')
            default_params = {
                'freq': week_freq,
                'title': 'Y / Y',
                'filter': week_condition,
                'pct_diff': incl_pct_diff,
                'rows': [
                    {
                        'name': '-'.join([this_week_ly_sun, this_week_ly_sat]),
                        'range': d.this_week_ly.to_period(week_freq).min(),
                        'agg': 'sum'
                    },
                    {
                        'name': f'{average_length}W Mean',
                        'range': d.prev_Nw_ly(average_length).to_period(week_freq).unique(),
                        'agg': ['sum', 'mean']
                    },
                    {
                        'name': '-'.join([this_week_sun, this_week_sat]),
                        'range': d.this_week.to_period(week_freq).min(),
                        'agg': 'sum'
                    }
                ]
            }
        if not avg_row:
            default_params['rows'] = [d for d in default_params.get('rows', []) if str(d.get('name', '')).lower() not in ['6w mean', '6w avg']]
    else:
        default_params = {}
    params = {**{k: v for k, v in default_params.items() if k not in params}, **params}
    if not params:
        raise ValueError("Pass through parameters or a table kind (['ytd','mtd','wow','yoy']).")
    default_params = {
        'pct_diff': True,
        'filter': None,
        'title': None,
        'freq': week_freq
    }
    for k, v in default_params.items():
        if k not in params:
            params[k] = v
    df_trim = df[params['filter']]
    df_result = pd.DataFrame()

    for row in params.get('rows', []):
        row['agg'] = [row.get('agg')] if isinstance(row.get('agg'), str) else row.get('agg', ['sum'])
        assert isinstance(row['agg'], list), TypeError('agg must be a list of aggregative methods, or simply the name of one.')
        for i, agg in enumerate(row.get('agg')):
            if i < 1:
                df_temp = df_trim.resample(params['freq'], kind='period').agg(agg)
                if hasattr(row.get('range'), 'tolist'):
                    df_temp = df_temp.reindex(row.get('range').tolist())
                else:
                    df_temp = df_temp.reindex([row.get('range')])
            else:
                df_temp = df_temp.fillna(0).agg(agg).to_frame().T
        df_temp.index = [row.get('name')]
        df_result = pd.concat([df_result, df_temp], sort=False)
    df_result.pipe(ratify, heads=heads)
    if params.get('pct_diff', True):
        df_result = df_result.pipe(pct_diff)
    df_result.index.name = params.get('title')
    return df_result

import alx.constants as const
import alx.data_manipulation as dm
import random

import pandas as pd

from typing import List, Union


__all__ = [
    'calculate_expected_value',
    'extend_with_forecast',
    'forecast_score_to_english'
]


def calculate_expected_value(row, df: pd.DataFrame, metric: str, min_factor: Union[float, int] = 0.5, max_factor: Union[float, int] = 2, sent_or_deliv: str = 'Delivered', weeks_back: int = 6):
    df_compare = (
        df
        .pipe(
            dm.date_filter,
            const.Dates(row['Date'])
            .prev_Nw(weeks_back, False)
        )
        .groupby(['Date', 'Mailing'])
        .sum()
    )
    vol_above_min = (df_compare[sent_or_deliv] >= row[sent_or_deliv] * min_factor)
    vol_below_max = (df_compare[sent_or_deliv] <= row[sent_or_deliv] * max_factor)
    df_compare = df_compare[vol_above_min & vol_below_max].sum()

    comp_metric = df_compare[metric]
    comp_volume = df_compare[sent_or_deliv]
    exp_volume = row[sent_or_deliv]

    if not (comp_metric and comp_volume):
        return 0
    else:
        return (comp_metric / comp_volume) * exp_volume


def extend_with_forecast(df: pd.DataFrame, df_compare: pd.DataFrame, metrics: List[str] = ['Opens', 'Clicks', 'Revenue'], col_prefix: str = 'Expected', scores: dict = {}, n: int = 10, ascending: bool = False, **kwargs):
    """
    Provided a primary DF filtered for campaigns and a focus time and a secondary df filtered for campaigns alone,
    determine the expected number of opens, clicks, etc.
    """

    def mailings_to_simple_analysis(df):
        df['High Score Column'] = df[metrics].abs().idxmax(axis=1)
        df['High Score'] = df.apply(lambda x: x[' '.join([x['High Score Column'], 'Score'])], axis=1).to_list()
        df_result = df.sort_values('High Score', ascending=ascending, key=abs)
        if n > 0:
            return df_result[:n]
        else:
            return df_result

    for metric in metrics:
        df[' '.join([col_prefix, metric])] = (
            df
            .reset_index()
            .apply(
                calculate_expected_value,
                axis=1,
                df=df_compare,
                metric=metric,
                **kwargs
            )
            .to_list()
        )
        df[' '.join([metric, 'Score'])] = scores.get(metric, 0) * (df[metric] - df[' '.join([col_prefix, metric])])
        df[' '.join([metric, 'Difference'])] = abs(df[metric] - df[' '.join([col_prefix, metric])])

    df = df.pipe(mailings_to_simple_analysis)
    return df


def forecast_score_to_english(row: pd.Series, id_col: str):
    SYNONYMS = {
        'verb': ['got', 'received', 'ended up with', 'yielded'],
        'more': ['more than', 'in excess of', 'above', 'beyond'],
        'less': ['less than', 'below', 'under'],
        'expected': ['expected', 'forcasted', 'calculated'],
    }
    return ' '.join([y for y in [
        row[id_col],
        random.sample(SYNONYMS.get('verb'), 1)[0],
        const.heads.search(row['High Score Column']).formatting.format(row[' '.join([row['High Score Column'], 'Difference'])]),
        None if 'Revenue' in row['High Score Column'] else row['High Score Column'],
        random.sample(SYNONYMS.get('more') if int(row[' '.join([row['High Score Column'], 'Score'])]) > 0 else SYNONYMS.get('less'), 1)[0],
        random.sample(SYNONYMS.get('expected'), 1)[0],
    ] if y is not None]) + '.'


"""
Demonstration of usage of the above:

# Organize the data we're inputting
df_c = df[(df['Type 0'] == 'Campaign') & (df['Type 1'] != 'Old Campaign')]
df_filter = df_c.pipe(alx.date_filter).groupby(['Date', 'Mailing']).sum()

# Gather the forecasted values (static week distance, static importance scores)
df_result = df_filter.pipe(extend_with_forecast, df_c, scores={
    'Opens': 3,
    'Clicks': 20,
    'Revenue': 5,
    'GA Revenue': 5
})

# Turn numbers into sentences
sentences = df_result.reset_index().apply(forecast_score_to_english, axis=1, id_col='Mailing').to_list()
"""

from alx.constants import *  # noqa
from alx.forecasting import *  # noqa
from alx.plotting import *  # noqa
from alx.data_manipulation import *  # noqa
from alx.util import *  # noqa

# module level docstring
__doc__ = """
alx_pydata is a bespoke python library for data analysis and reporting
at Alchemy Worx NYC.

Its operation is built primarily on top of the pandas dataframe library.
It is required that the primary df used for manipulation has a datetimeindex.
"""

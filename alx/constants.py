import datetime as dt
import re

import numpy as np
import pandas as pd

from enum import Enum
from itertools import repeat
from typing import List, Optional, Tuple, Union

from dateutil.parser import parse
from pydantic import BaseModel, validator


__all__ = [
    'us_week',
    'date_from_parts',
    'weekly_comp_table_by_year',
    'Dates',
    'date_from_iso',
    'sun',
    'sat',
    'this_week',
    'last_week',
    'prev_6w',
    'sun_ly',
    'sat_ly',
    'this_week_ly',
    'last_week_ly',
    'prev_6w_ly',
    'headers',  # DEPRECATED
    'format_dict',
    'column_format',  # DEPRECATED
    'default_export_range',
    'Format',
    'Field',
    'Heads',
    'heads'
]

# \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
# |||||||||||||||||||  D A T E  D E F I N I T I O N  |||||||||||||||||||||||||||
# //////////////////////////////////////////////////////////////////////////////


def us_week(ts: Union[pd.Timestamp, dt.date, dt.datetime, str]) -> int:
    """Calculate Standard American Gregorian Traditional Work Week Number
    Leaning on the STRFTIME format "%U", where each week begins on Sunday,
    this function adjusts week numbers such that every year's January 1st is in Week 1
    while also adjusting any evidence of 0-based indexing.

    Arguments:
        ts {Union[pd.Timestamp, dt.date, dt.datetime, str]} -- Timestamp, date, or otherwise.

    Returns:
        int -- Week Number
    """
    if pd.isnull(ts):
        return np.nan
    else:
        if isinstance(ts, str):
            ts = parse(ts)
        U = int(ts.strftime('%U'))

    # %U dictates that the first Sunday of the year is Week 1, and preceding days in the year are Week 0.
    # This implies that the previous year ends incompletely.
    # If this date is in one of these incomplete year-end weeks, make this part of the next years first week.
    this_week_is_last_of_year = (int(dt.datetime(ts.year, 12, 31).strftime('%U')) == U)
    next_year_starts_week_0 = (int(dt.datetime(ts.year + 1, 1, 1).strftime('%U')) == 0)
    if this_week_is_last_of_year and next_year_starts_week_0:
        return 1

    # Some years start with W1 not W0 (eg. 2017, where 1/1 is a Sunday.)
    # If so, U corresponds to the North American work week already
    elif int(dt.datetime(ts.year, 1, 1).strftime('%U')) == 1:
        return U
    # %U's tendency to index by 0 in years with "imperfect" starts is an issue.
    else:
        return U + 1


def date_from_parts(
    year: int,
    week: int,
    day: int,
    *,
    freq: str = 'W-SAT'
) -> pd.Timestamp:
    """Obtain date, provided descriptors for its year, week, and date.
    Constraints which lie out of typical bounds are rolled over into the next applicable field.
    January 1 is always in Week 1 of a given year.

    Eg.
    date_from_parts(
        year=2020,
        week=54,
        day=8,
    )
    > Timestamp('2021-01-11 00:00:00', freq='D')
    # This is a Monday

    Arguments:
        year {int} -- Year of interest
        week {int} -- Week of year
        day {int} -- Day of week (Sunday = 0)

    Keyword Arguments:
        freq {str} -- Weekly Frequency collection string (default: {'W-SAT'})

    Returns:
        pd.Timestamp -- date of interest
    """

    days = pd.date_range(
        dt.date(year, 1, 1),
        dt.date(year + (week // 52), 12, 31),
        freq=freq
    )

    week_periods_by_week_number = pd.Series(
        days.to_period(freq),
    )

    week += day // 7

    return pd.date_range(
        week_periods_by_week_number[week - 1].start_time,
        week_periods_by_week_number[week - 1].end_time
    )[day % 7]


def weekly_comp_table_by_year(
    start_year: int,
    end_year: int,
    *,
    freq: str = 'W-SAT',
    index_name: str = None,
    columns_name: str = 'Year',
    square: bool = False,
    bump_index: bool = False
) -> pd.DataFrame:
    """Orderly comparison weeks split by year

    Arguments:
        start_year {int} -- Year to start comparison table
        end_year {int} -- Year to end comparison table

    Keyword Arguments:
        freq {str} -- Weekly Frequency string by which to consolidate dates (default: {'W-SAT'})
        index_name {str} -- Name for the index column (default: {None})
        columns_name {str} -- Name for the columns set (default: {'Year'})
        square {bool} -- Trim NA values off table (default: {False})
        bump_index {bool} -- Increase index values by +1. No 0-based indexing.

    Returns:
        pd.DataFrame -- Arbitrary Week index, Years as columns, periods as values
    """

    days = pd.date_range(
        dt.date(int(start_year), 1, 1),
        dt.date(int(end_year), 12, 31),
        freq=freq
    )

    week_val = 0
    limit = None
    if freq.upper().startswith('W'):
        week_val = pd.Series(days).apply(us_week).to_list()
        limit = 53
        index_name = index_name or 'Week'
    elif freq.upper() in ['B', 'C', 'D']:
        limit = 366
        index_name = index_name or 'Day'
    elif freq.upper().startswith('M'):
        index_name = index_name or 'Month'

    df_week_nums = pd.DataFrame(
        data={
            'year': days.year,
            'week': week_val,
        },
        index=days.to_period(freq)
    )
    num_weeks_by_year = df_week_nums.groupby('year')['week'].max()
    padding = (num_weeks_by_year < num_weeks_by_year.shift()).cumsum()

    df_result = pd.DataFrame()
    for i, year in enumerate(sorted(days.year.unique())):
        df_result = pd.concat(
            [
                df_result,
                pd.DataFrame(
                    [
                        *([pd.NaT] * padding[year]),
                        *df_week_nums[df_week_nums['year'] >= year].index.to_list()
                    ][:(None if not limit else (limit + padding.max()))],
                    columns=[year]
                )
            ],
            axis=1)

    # Added on FEB 2024
    if freq=='W-SUN':
        year_column = [col for col in df_result.columns if '2023' in str(col)]
        if year_column:
            df_result[year_column[0]] = df_result[year_column[0]].shift(-1)
            df_result = df_result.iloc[:-1]
        else:
            print("Column '2023' not found in the DataFrame.")

    if square:
        df_result = (
            df_result
            .fillna(pd.NaT)
            .dropna(subset=[df_result.columns.max()])
            .reset_index(drop=True)
        )
    else:
        df_result = df_result.fillna(pd.NaT)

    df_result.index.name = index_name
    df_result.columns.name = columns_name

    if bump_index:
        df_result.index = df_result.index + 1

    return df_result


class Dates():
    def __init__(
        self,
        date: Union[dt.date, dt.datetime, pd.Timestamp, str] = None,
        *,
        year: int = None,
        week: int = None,
        day: int = None,  # Sunday is 0
        week_end: str = 'Saturday',
    ):
        """Alchemy Worx Dates Object; best for managing references to comparable dates

        Keyword Arguments:
            date {Union[dt.date, dt.datetime, pd.Timestamp, str]} -- Optional: declare the focus date as a date object (default: {None})
            year {int} -- Optional: declare focus year. Must be passed with week and day. (default: {None})
            week {int} -- Optional: declare focus week. Must be passed with year and day. (default: {None})
            day {int} -- Optional: declare focus day index. Must be passed with year and week (default: {None})

        Raises:
            ValueError: Must provide only date, or any of year, week, day.
        """

        if all([date, any([year, week, day])]):
            raise ValueError('Either provide only a date object, or any combination of [year, week, day].')

        self.freq = f'W-{week_end.upper()[:3]}'
        self.date = date or dt.date.today()

        if any([year, week, day]):
            self.year = year or date.year
            self.week = week or us_week(date)
            self.day = day or (date.weekday() + 1)
            self.date = date_from_parts(year, week, day, freq=self.freq)
        else:
            if isinstance(self.date, str):
                self.date = parse(date)
            self.year = self.date.year
            self.week = us_week(self.date)
            self.day = self.date.weekday() + 1

        self.this_week = self.prev_Nw(1, False)
        self.last_week = self.prev_Nw(1)
        self.prev_6w = self.prev_Nw(6)
        self.sun = self.this_week.min().date()
        self.sat = self.this_week.max().date()

        df_year_comp = weekly_comp_table_by_year(
            self.this_week.max().year - 1,
            self.this_week.max().year,
            freq=self.freq
        )
        comparable_year = df_year_comp.loc[
            df_year_comp[self.this_week.max().year] == pd.Period(self.this_week.max(), freq=self.freq), (self.this_week.max().year - 1)].iloc[0]

        self.this_week_ly = self.period_to_date_range(comparable_year)
        self.sun_ly = self.this_week_ly.min().date()
        self.sat_ly = self.this_week_ly.max().date()
        self.last_week_ly = self.prev_Nw(1, False, date=self.sat_ly)
        self.prev_6w_ly = self.prev_Nw(6, False, date=self.sat_ly)

    def set_date(self, *args, **kwargs):
        self.__init__(*args, **kwargs)
        return self

    def reset_dates(self):
        self.__init__()
        return self

    def prev_Nw(self, n: int, skip_this_week: bool = True, *, date: Union[dt.date, dt.datetime, pd.Timestamp, str] = None) -> pd.date_range:
        if n < 0:
            raise ValueError('n must be greater than 0.')
        if date is None:
            date = self.date
        elif isinstance(date, str):
            date = parse(date)

        period = pd.Timestamp(date - dt.timedelta(weeks=1)).to_period(self.freq)
        return pd.date_range(
            period.start_time - dt.timedelta(weeks=(n - int(not skip_this_week))),
            period.end_time - dt.timedelta(weeks=int(skip_this_week))
        )

    def prev_Nw_ly(self, *args, **kwargs):
        return self.prev_Nw(*args, date=self.sun_ly, **kwargs)

    def period_to_date_range(self, period: pd.Period) -> pd.date_range:
        return pd.date_range(period.start_time, period.end_time)


# \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
# |||||||||||||||||||  D A T E  D E F I N I T I O N  |||||||||||||||||||||||||||
# //////////////////////////////////////////////////////////////////////////////


# DEPRECATED
def date_from_iso(iso_year: int, iso_week: int, iso_day: int) -> dt.date:
    """Return Gregorian values, given ISO date objects.

    Arguments:
        iso_year {int} -- year
        iso_week {int} -- week
        iso_day {int} -- day of week (0 = Sun)

    Returns:
        dt.date -- datetime formatted date
    """

    def _iso_year_start(iso_year: int) -> dt.date:
        """Determine the start of the year, given the year of interest.

        Arguments:
            iso_year {int} -- year

        Returns:
            dt.date -- standard date of start of provided year
        """

        fourth_jan = dt.date(iso_year, 1, 4)
        delta = dt.timedelta(fourth_jan.isoweekday() - 1)
        return fourth_jan - delta
    year_start = _iso_year_start(iso_year)
    return year_start + dt.timedelta(days=iso_day - 1, weeks=iso_week - 1)


_default_dates = Dates().reset_dates()
sun = _default_dates.sun
sat = _default_dates.sat
this_week = _default_dates.this_week
last_week = _default_dates.last_week
prev_6w = _default_dates.prev_6w

sun_ly = _default_dates.sun_ly
sat_ly = _default_dates.sat_ly
this_week_ly = _default_dates.this_week_ly
last_week_ly = _default_dates.last_week_ly
prev_6w_ly = _default_dates.prev_6w_ly

default_export_range = pd.date_range(min(prev_6w), max(this_week))

# \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
# |||||||||||||||||||  S T D  D E F I N I T I O N S  |||||||||||||||||||||||||||
# //////////////////////////////////////////////////////////////////////////////

# DEPRECATED (Use of all, _all, _op, all_adobe, adobe_esp, nega_op no longer in use.)
headers = {
    'all': [
        'Delivered',
        'Opens',
        'Opens / Delivered',
        'Clicks',
        'Clicks / Delivered',
        'Clicks / Open',
        'Revenue',
        'Orders',
        'AOV',
        'Conversion',
        'Unsub',
        'Unsub / Delivered',
        'eCPM',
        'GA Revenue',
        'GA Orders',
        'GA Sessions',
        'GA AOV',
        'GA Conversion',
        'GA Orders / Session',
        'GA eCPM'
    ],
    'all_adobe': [
        'Delivered',
        'Opens',
        'Opens / Delivered',
        'Clicks',
        'Clicks / Delivered',
        'Clicks / Open',
        'Revenue',
        'Orders',
        'AOV',
        'Conversion',
        'Unsub',
        'Unsub / Delivered',
        'eCPM',
        'GA Revenue',
        'GA Orders',
        'GA Sessions',
        'GA AOV',
        'GA Conversion',
        'GA Orders / Session',
        'GA eCPM',
        'AA Revenue',
        'AA Orders',
        'AA Sessions',
        'AA AOV',
        'AA Conversion',
        'AA Orders / Session',
        'AA eCPM'
    ],
    'esp': [
        'Delivered',
        'Opens',
        'Opens / Delivered',
        'Clicks',
        'Clicks / Delivered',
        'Clicks / Open',
        'Revenue',
        'Orders',
        'AOV',
        'Conversion',
        'Unsub',
        'Unsub / Delivered',
        'eCPM',
    ],
    'ga': [
        'Delivered',
        'Opens',
        'Opens / Delivered',
        'Clicks',
        'Clicks / Delivered',
        'Clicks / Open',
        'GA Revenue',
        'GA Orders',
        'GA Sessions',
        'GA AOV',
        'GA Conversion',
        'GA Orders / Session',
        'Unsub',
        'Unsub / Delivered',
        'GA eCPM'
    ],
    'adobe': [
        'Delivered',
        'Opens',
        'Opens / Delivered',
        'Clicks',
        'Clicks / Delivered',
        'Clicks / Open',
        'AA Revenue',
        'AA Orders',
        'AA Sessions',
        'AA AOV',
        'AA Conversion',
        'AA Orders / Session',
        'Unsub',
        'Unsub / Delivered',
        'AA eCPM'
    ],
    'adobe_esp': [
        'Delivered',
        'Opens',
        'Opens / Delivered',
        'Clicks',
        'Clicks / Delivered',
        'Clicks / Open',
        'Revenue',
        'Orders',
        'AOV',
        'Conversion',
        'eCPM',
        'AA Revenue',
        'AA Orders',
        'AA Sessions',
        'AA AOV',
        'AA Conversion',
        'AA Orders / Session',
        'Unsub',
        'Unsub / Delivered',
        'AA eCPM'
    ],
    'op': [
        'Delivered',
        'Opens',
        'Clicks',
        'Revenue',
        'Orders',
        'Unsub',
        'GA Revenue',
        'GA Orders',
        'GA Sessions',
        'AA Revenue',
        'AA Orders',
        'AA Sessions',
    ],
    'eng': [
        'Delivered',
        'Opens',
        'Opens / Delivered',
        'Clicks',
        'Clicks / Delivered',
        'Clicks / Open',
        'Unsub',
        'Unsub / Delivered',
    ],
    'nega': [
        'Unsub',
        'Unsub / Delivered',
        'Complaints',
        'Complaints / Delivered',
        'Hard Bounces',
        'Soft Bounces',
        'Total Bounces',
        'Total Bounces / Delivered'
    ],
    'nega_op': [
        'Unsub',
        'Complaints',
        'Hard Bounces',
        'Soft Bounces',
        'Total Bounces'
    ],
    '_all': [
        'Sent',
        'Delivered',
        'Opens',
        'Opens / Delivered',
        'Clicks',
        'Clicks / Delivered',
        'Clicks / Open',
        'Revenue',
        'Orders',
        'AOV',
        'Conversion',
        'eCPM',
        'GA Revenue',
        'GA Orders',
        'GA Sessions',
        'GA AOV',
        'GA Conversion',
        'GA Orders / Session',
        'GA eCPM',
        'AA Revenue',
        'AA SFCC Demand',
        'AA Orders',
        'AA Sessions',
        'AA Visits'
        'AA AOV',
        'AA Conversion',
        'AA Orders / Session',
        'AA Orders / Visit',
        'AA Visits / Open',
        'AA eCPM',
        'Margin Dollars',
        'Unsub',
        'Unsub / Delivered',
        'Complaints',
        'Complaints / Delivered',
        'Hard Bounces',
        'Soft Bounces',
        'Total Bounces',
        'Total Bounces / Delivered'
    ],
    '_op': [
        'Sent',
        'Delivered',
        'Opens',
        'Clicks',
        'Revenue',
        'Orders',
        'GA Revenue',
        'GA Orders',
        'GA Sessions',
        'AA Revenue',
        'AA SFCC Demand',
        'AA Orders',
        'AA Sessions',
        'AA Visits',
        'Margin Dollars',
        'Unsub',
        'Complaints',
        'Hard Bounces',
        'Soft Bounces',
        'Total Bounces'
    ],
    'fbb': [
        'Delivered',
        'Opens',
        'Opens / Delivered',
        'Clicks',
        'Clicks / Delivered',
        'Clicks / Open',
        'Revenue',
        'Orders',
        'AOV',
        'Conversion',
        'eCPM',
        'AA Revenue',
        'AA SFCC Demand',
        'AA Orders',
        'AA Visits',
        'AA AOV',
        'AA Conversion',
        'AA Orders / Visit',
        'AA Visits / Open',
        'Unsub',
        'Unsub / Delivered',
        'AA eCPM',
        'Margin Dollars',
        'Margin Percent'
    ]
}


# DEPRECATED
format_dict = {
    'number': '{:,.0f}',
    'percent': '{:,.2%}',
    'dollar': '${:,.0f}',
    'euro': '€{:,.0f}',
    'date': '{:%-m/%-d/%y}'
}


# DEPRECATED
column_format = {
    'Sent': format_dict['number'],
    'Count': format_dict['number'],
    'Delivered': format_dict['number'],
    'Opens': format_dict['number'],
    'Opens / Delivered': format_dict['percent'],
    'Clicks': format_dict['number'],
    'Clicks / Delivered': format_dict['percent'],
    'Clicks / Open': format_dict['percent'],
    'Revenue': format_dict['dollar'],
    'Orders': format_dict['number'],
    'AOV': format_dict['dollar'],
    'Conversion': format_dict['percent'],
    'Orders / Delivered': format_dict['percent'],
    'Unsub': format_dict['number'],
    'Unsub / Delivered': format_dict['percent'],
    'eCPM': format_dict['dollar'],
    'GA Revenue': format_dict['dollar'],
    'GA Orders': format_dict['number'],
    'GA Sessions': format_dict['number'],
    'GA AOV': format_dict['dollar'],
    'GA Conversion': format_dict['percent'],
    'GA Orders / Delivered': format_dict['percent'],
    'GA Orders / Session': format_dict['percent'],
    'GA eCPM': format_dict['dollar'],
    'AA Revenue': format_dict['dollar'],
    'AA SFCC Demand': format_dict['dollar'],
    'AA Orders': format_dict['number'],
    'AA Sessions': format_dict['number'],
    'AA AOV': format_dict['dollar'],
    'AA Conversion': format_dict['percent'],
    'AA Orders / Session': format_dict['percent'],
    'AA Orders / Visit': format_dict['percent'],
    'AA Visits / Open': format_dict['percent'],
    'AA eCPM': format_dict['dollar'],
    'Complaints': format_dict['number'],
    'Complaints / Delivered': format_dict['percent'],
    'Hard Bounces': format_dict['number'],
    'Soft Bounces': format_dict['number'],
    'Total Bounces': format_dict['number'],
    'Total Bounces / Delivered': format_dict['percent'],
    'Margin Dollars': format_dict['dollar'],
    'Margin Percent': format_dict['percent'],
    **format_dict
}


class Format(str, Enum):
    number: str = '{:,.0f}'
    percent: str = '{:,.2%}'
    dollar: str = '${:,.0f}'
    euro: str = '€{:,.0f}'
    date: str = '{:%-m/%-d/%y}'
    basis_point: str = '{:,.0f} BP'


class FieldType(str, Enum):
    string: str = 'STRING'
    bytes: str = 'BYTES'
    integer: str = 'INTEGER'
    float: str = 'FLOAT'
    boolean: str = 'BOOLEAN'
    timestamp: str = 'TIMESTAMP'
    date: str = 'DATE'
    time: str = 'TIME'
    datetime: str = 'DATETIME'
    record: str = 'RECORD'
    struct: str = 'STRUCT'


class CastType(str, Enum):
    string: str = 'STRING'
    bytes: str = 'BYTES'
    int64: str = 'INT64'
    float64: str = 'FLOAT64'
    bool: str = 'BOOL'
    timestamp: str = 'TIMESTAMP'
    date: str = 'DATE'
    time: str = 'TIME'
    datetime: str = 'DATETIME'
    array: str = 'ARRAY'
    struct: str = 'STRUCT'


class SqlMode(str, Enum):
    nullable: str = 'NULLABLE'
    required: str = 'REQUIRED'
    repeated: str = 'REPEATED'


class Field(BaseModel):
    name: str
    short_name: str
    sql_name: str
    order: float
    formatting: Optional[Union[Format, str]]
    subset: List[str] = []
    derived_columns: Optional[Tuple[str, str]] = None
    multiplier: float = 1
    sql_mode: SqlMode = SqlMode.nullable
    field_type: Optional[FieldType] = None
    cast_type: Optional[CastType] = None
    description: Optional[str] = None
    fbb_name: Optional[str] = None

    class Config:
        use_enum_values = True

    def __str__(self):
        return self.name

    @validator('fbb_name')
    def default_fbb_name(cls, v, values, **kwargs):
        return v or values['name']


class Heads(object):
    def __init__(self, currency: Union[str, Format] = Format.dollar):
        if not currency:
            currency = Format.dollar
        elif isinstance(currency, Format):
            pass
        elif isinstance(currency, str):
            try:
                currency = Format[currency]
            except KeyError:
                raise ValueError(f'{currency} is not a valid currency-format option.')
        else:
            raise ValueError(f'currency is incorrect format: {type(currency)}')

        self.Date = Field(
            name='Date',
            short_name='Date',
            sql_name='Date',
            order=0,
            formatting=Format.date,
            subset=['xlsx', 'id'],
            sql_mode='REQUIRED',
            field_type='STRING',
            cast_type='DATE'
        )
        self.Name = Field(
            name='Name',
            short_name='Name',
            sql_name='Name',
            order=1,
            formatting=None,
            subset=['utility', 'id'],
            sql_mode='REQUIRED',
            field_type='STRING',
            cast_type='STRING'
        )
        self.Campaign = Field(
            name='Campaign',
            short_name='Campaign',
            sql_name='Campaign',
            order=2,
            formatting=None,
            subset=['xlsx', 'id'],
            sql_mode='NULLABLE',
            field_type='STRING',
            cast_type='STRING'
        )
        self.Mailing = Field(
            name='Mailing',
            short_name='Mailing',
            sql_name='Mailing',
            order=3,
            formatting=None,
            subset=['xlsx', 'id'],
            sql_mode='NULLABLE',
            field_type='STRING',
            cast_type='STRING'
        )
        self.Variant = Field(
            name='Variant',
            short_name='Variant',
            sql_name='Variant',
            order=4,
            formatting=None,
            subset=['xlsx', 'id'],
            sql_mode='NULLABLE',
            field_type='STRING',
            cast_type='STRING'
        )
        self.Subject_Line = Field(
            name='Subject Line',
            short_name='Subject',
            sql_name='Subject_Line',
            order=5,
            formatting=None,
            subset=['xlsx', 'id'],
            sql_mode='NULLABLE',
            field_type='STRING',
            cast_type='STRING'
        )
        self.Segment = Field(
            name='Segment',
            short_name='Segment',
            sql_name='Segment',
            order=6,
            formatting=None,
            subset=['xlsx', 'id'],
            sql_mode='NULLABLE',
            field_type='STRING',
            cast_type='STRING'
        )
        self.Type_0 = Field(
            name='Type 0',
            short_name='Type 0',
            sql_name='Type_0',
            order=7,
            formatting=None,
            subset=['utility', 'id'],
            sql_mode='REQUIRED',
            field_type='STRING',
            cast_type='STRING'
        )
        self.Type_1 = Field(
            name='Type 1',
            short_name='Type 1',
            sql_name='Type_1',
            order=8,
            formatting=None,
            subset=['utility', 'id'],
            sql_mode='NULLABLE',
            field_type='STRING',
            cast_type='STRING'
        )
        self.Type_2 = Field(
            name='Type 2',
            short_name='Type 2',
            sql_name='Type_2',
            order=9,
            formatting=None,
            subset=['utility', 'id'],
            sql_mode='NULLABLE',
            field_type='STRING',
            cast_type='STRING'
        )
        self.Offer = Field(
            name='Offer',
            short_name='Offer',
            sql_name='Offer',
            order=10,
            formatting=None,
            subset=['utility'],
            sql_mode='NULLABLE',
            field_type='STRING',
            cast_type='STRING'
        )
        self.Segment_Engagement = Field(
            name='Segment Engagement',
            short_name='Seg Eng',
            sql_name='Segment_Engagement',
            order=11,
            formatting=None,
            subset=['utility'],
            sql_mode='NULLABLE',
            field_type='STRING',
            cast_type='STRING'
        )
        self.Segment_BNB = Field(
            name='Segment BNB',
            short_name='Seg BNB',
            sql_name='Segment_BNB',
            order=12,
            formatting=None,
            subset=['utility'],
            sql_mode='NULLABLE',
            field_type='STRING',
            cast_type='STRING'
        )
        self.Segment_Freq = Field(
            name='Segment Freq',
            short_name='Seg Freq',
            sql_name='Segment_Freq',
            order=12.5,
            formatting=None,
            subset=['utility'],
            sql_mode='NULLABLE',
            field_type='STRING',
            cast_type='STRING'
        )
        self.Test_Type = Field(
            name='Test Type',
            short_name='Test Type',
            sql_name='Test_Type',
            order=13,
            formatting=None,
            subset=['utility'],
            sql_mode='NULLABLE',
            field_type='STRING',
            cast_type='STRING'
        )
        self.Error = Field(
            name='Error',
            short_name='Error',
            sql_name='Error',
            order=14,
            formatting=None,
            subset=['utility'],
            sql_mode='NULLABLE',
            field_type='STRING',
            cast_type='STRING'
        )
        self.Sent = Field(
            name='Sent',
            short_name='Sent',
            sql_name='Sent',
            order=15,
            formatting=Format.number,
            subset=['op'],
            sql_mode='NULLABLE',
            field_type='INTEGER',
            cast_type='FLOAT64'
        )
        self.Delivered = Field(
            name='Delivered',
            short_name='Deliv',
            sql_name='Delivered',
            order=16,
            formatting=Format.number,
            subset=['esp', 'engagement', 'op', 'fbb', 'aa', 'all', 'esp_no_rev', 'sms','aa_only'],
            sql_mode='NULLABLE',
            field_type='INTEGER',
            cast_type='FLOAT64'
        )
        self.Opens = Field(
            name='Opens',
            short_name='Open',
            sql_name='Opens',
            order=17,
            formatting=Format.number,
            subset=['esp', 'engagement', 'op', 'fbb', 'aa', 'all', 'esp_no_rev','aa_only'],
            sql_mode='NULLABLE',
            field_type='INTEGER',
            cast_type='FLOAT64'
        )
        self.Opens_per_Delivered = Field(
            name='Opens / Delivered',
            short_name='Open / Deliv',
            sql_name='Opens_per_Delivered',
            order=18,
            formatting=Format.percent,
            subset=['esp', 'engagement', 'rate', 'fbb', 'aa', 'all', 'esp_no_rev','aa_only'],
            derived_columns=('Opens', 'Delivered'),
            sql_mode='NULLABLE',
            field_type='FLOAT',
            cast_type='FLOAT64',
            fbb_name='Open Rate'
        )
        self.Clicks = Field(
            name='Clicks',
            short_name='Clicks',
            sql_name='Clicks',
            order=19,
            formatting=Format.number,
            subset=['esp', 'engagement', 'op', 'fbb', 'aa', 'all', 'esp_no_rev', 'sms','aa_only'],
            sql_mode='NULLABLE',
            field_type='FLOAT',
            cast_type='FLOAT64'
        )
        self.Clicks_per_Delivered = Field(
            name='Clicks / Delivered',
            short_name='Click / Deliv',
            sql_name='Clicks_per_Delivered',
            order=20,
            formatting=Format.percent,
            subset=['esp', 'engagement', 'rate', 'aa', 'all', 'esp_no_rev', 'sms','aa_only'],
            derived_columns=('Clicks', 'Delivered'),
            sql_mode='NULLABLE',
            field_type='FLOAT',
            cast_type='FLOAT64'
        )
        self.Clicks_per_Open = Field(
            name='Clicks / Open',
            short_name='Click / Open',
            sql_name='Clicks_per_Open',
            order=21,
            formatting=Format.percent,
            subset=['esp', 'engagement', 'rate', 'aa', 'all', 'esp_no_rev','aa_only'],
            derived_columns=('Clicks', 'Opens'),
            sql_mode='NULLABLE',
            field_type='FLOAT',
            cast_type='FLOAT64'
        )
        self.Revenue = Field(
            name='Revenue',
            short_name='Rev',
            sql_name='Revenue',
            order=22,
            formatting=currency,
            subset=['esp', 'revenue', 'op', 'all', 'sms'],
            sql_mode='NULLABLE',
            field_type='FLOAT',
            cast_type='FLOAT64'
        )
        self.Orders = Field(
            name='Orders',
            short_name='Order',
            sql_name='Orders',
            order=23,
            formatting=Format.number,
            subset=['esp', 'revenue', 'op', 'all', 'sms'],
            sql_mode='NULLABLE',
            field_type='INTEGER',
            cast_type='FLOAT64'
        )
        self.AOV = Field(
            name='AOV',
            short_name='AOV',
            sql_name='AOV',
            order=24,
            formatting=currency,
            subset=['esp', 'revenue', 'rate', 'all', 'sms'],
            derived_columns=('Revenue', 'Orders'),
            sql_mode='NULLABLE',
            field_type='FLOAT',
            cast_type='FLOAT64'
        )
        self.Conversion = Field(
            name='Conversion',
            short_name='Conv',
            sql_name='Conversion',
            order=25,
            formatting=Format.percent,
            subset=['esp', 'revenue', 'rate', 'all', 'sms'],
            derived_columns=('Orders', 'Clicks'),
            sql_mode='NULLABLE',
            field_type='FLOAT',
            cast_type='FLOAT64'
        )
        self.Orders_per_Delivered = Field(
            name='Orders / Delivered',
            short_name='Orders / Deliv',
            sql_name='Orders_per_Delivered',
            order=25.5,
            formatting=Format.percent,
            subset=['revenue', 'rate', 'karity', 'sms'],
            derived_columns=('Orders', 'Delivered'),
            sql_mode='NULLABLE',
            field_type='FLOAT',
            cast_type='FLOAT64'
        )
        self.eCPM = Field(
            name='eCPM',
            short_name='eCPM',
            sql_name='eCPM',
            order=26,
            formatting=currency,
            subset=['esp', 'revenue', 'rate', 'all', 'sms'],
            derived_columns=('Revenue', 'Delivered'),
            multiplier=1000,
            sql_mode='NULLABLE',
            field_type='FLOAT',
            cast_type='FLOAT64'
        )
        self.GA_Revenue = Field(
            name='GA Revenue',
            short_name='GA Rev',
            sql_name='GA_Revenue',
            order=27,
            formatting=currency,
            subset=['ga', 'revenue', 'op', 'all'],
            sql_mode='NULLABLE',
            field_type='FLOAT',
            cast_type='FLOAT64'
        )
        self.GA_Orders = Field(
            name='GA Orders',
            short_name='GA Order',
            sql_name='GA_Orders',
            order=28,
            formatting=Format.number,
            subset=['ga', 'revenue', 'op', 'all'],
            sql_mode='NULLABLE',
            field_type='INTEGER',
            cast_type='FLOAT64'
        )
        self.GA_Sessions = Field(
            name='GA Sessions',
            short_name='GA Sess',
            sql_name='GA_Sessions',
            order=29,
            formatting=Format.number,
            subset=['ga', 'engagement', 'op', 'all'],
            sql_mode='NULLABLE',
            field_type='INTEGER',
            cast_type='FLOAT64'
        )
        self.GA_AOV = Field(
            name='GA AOV',
            short_name='GA AOV',
            sql_name='GA_AOV',
            order=30,
            formatting=currency,
            subset=['ga', 'revenue', 'rate', 'all'],
            derived_columns=('GA Revenue', 'GA Orders'),
            sql_mode='NULLABLE',
            field_type='FLOAT',
            cast_type='FLOAT64'
        )
        self.GA_Conversion = Field(
            name='GA Conversion',
            short_name='GA Conv',
            sql_name='GA_Conversion',
            order=31,
            formatting=Format.percent,
            subset=['ga', 'revenue', 'rate', 'all'],
            derived_columns=('GA Orders', 'Clicks'),
            sql_mode='NULLABLE',
            field_type='FLOAT',
            cast_type='FLOAT64'
        )
        self.GA_Orders_per_Delivered = Field(
            name='GA Orders / Delivered',
            short_name='GA Order / Deliv',
            sql_name='GA_Orders_per_Delivered',
            order=32,
            formatting=Format.percent,
            subset=['revenue', 'rate', 'karity'],
            derived_columns=('GA Orders', 'Delivered'),
            sql_mode='NULLABLE',
            field_type='FLOAT',
            cast_type='FLOAT64'
        )
        self.GA_Orders_per_Session = Field(
            name='GA Orders / Session',
            short_name='GA Order / Sess',
            sql_name='GA_Orders_per_Session',
            order=33,
            formatting=Format.percent,
            subset=['ga', 'revenue', 'rate', 'all'],
            derived_columns=('GA Orders', 'GA Sessions'),
            sql_mode='NULLABLE',
            field_type='FLOAT',
            cast_type='FLOAT64'
        )
        self.GA_eCPM = Field(
            name='GA eCPM',
            short_name='GA eCPM',
            sql_name='GA_eCPM',
            order=34,
            formatting=currency,
            subset=['ga', 'revenue', 'rate', 'all'],
            derived_columns=('GA Revenue', 'Delivered'),
            multiplier=1000,
            sql_mode='NULLABLE',
            field_type='FLOAT',
            cast_type='FLOAT64'
        )
        self.AA_Visits = Field(
            name='AA Visits',
            short_name='AA Visit',
            sql_name='AA_Visits',
            order=34.05,
            formatting=Format.number,
            subset=['aa', 'fbb', 'op','aa_only'],
            sql_mode='NULLABLE',
            field_type='INTEGER',
            cast_type='FLOAT64'
        )
        self.AA_Visits_per_Open = Field(
            name='AA Visits / Open',
            short_name='AA Visit / Open',
            sql_name='AA_Visits_per_Open',
            order=34.1,
            formatting=Format.percent,
            subset=['aa', 'engagement', 'fbb', 'rate','aa_only'],
            derived_columns=('AA Visits', 'Opens'),
            sql_mode='NULLABLE',
            field_type='FLOAT',
            cast_type='FLOAT64',
            fbb_name='AA CTO%'
        )
        self.AA_Visits_per_Delivered = Field(
            name='AA Visits / Delivered',
            short_name='AA Visit / Del',
            sql_name='AA_Visits_per_Delivered',
            order=34.2,
            formatting=Format.percent,
            subset=['aa', 'engagement', 'fbb', 'rate','aa_only'],
            derived_columns=('AA Visits', 'Delivered'),
            sql_mode='NULLABLE',
            field_type='FLOAT',
            cast_type='FLOAT64',
            fbb_name='CTR%'
        )
        self.AA_Mobile_Visits = Field(
            name='AA Mobile Visits',
            short_name='AA Mobile Visit',
            sql_name='AA_Mobile_Visits',
            order=39.1,
            formatting=Format.number,
            subset=['aa', 'op', 'fbb'],
            sql_mode='NULLABLE',
            field_type='INTEGER',
            cast_type='FLOAT64'
        )
        self.AA_Mobile_Visits_Rate = Field(
            name='AA Mobile Visits %',
            short_name='AA Mobile %',
            sql_name='AA_Mobile_Visits_Rate',
            order=39.2,
            formatting=Format.percent,
            subset=['aa', 'rate', 'fbb'],
            derived_columns=('AA Mobile Visits', 'AA Visits'),
            sql_mode='NULLABLE',
            field_type='INTEGER',
            cast_type='FLOAT64'
        )
        self.AA_Revenue = Field(
            name='AA Revenue',
            short_name='AA Rev',
            sql_name='AA_Revenue',
            order=35,
            formatting=currency,
            subset=['aa', 'revenue', 'fbb', 'op','aa_only'],
            sql_mode='NULLABLE',
            field_type='FLOAT',
            cast_type='FLOAT64'
        )
        self.AA_SFCC_Demand = Field(
            name='AA SFCC Demand',
            short_name='AA SFCC Dem',
            sql_name='AA_SFCC_Demand',
            order=36,
            formatting=currency,
            subset=['fbb', 'revenue', 'op', 'aa'],
            sql_mode='NULLABLE',
            field_type='FLOAT',
            cast_type='FLOAT64',
            fbb_name='AA Demand'
        )
        self.AA_Orders = Field(
            name='AA Orders',
            short_name='AA Order',
            sql_name='AA_Orders',
            order=37,
            formatting=Format.number,
            subset=['aa', 'revenue', 'op', 'fbb','aa_only'],
            sql_mode='NULLABLE',
            field_type='INTEGER',
            cast_type='FLOAT64'
        )
        self.AA_AOV = Field(
            name='AA AOV',
            short_name='AA AOV',
            sql_name='AA_AOV',
            order=40,
            formatting=currency,
            subset=['aa', 'revenue', 'rate','aa_only'],
            derived_columns=('AA Revenue', 'AA Orders'),
            sql_mode='NULLABLE',
            field_type='FLOAT',
            cast_type='FLOAT64'
        )
        self.AA_SFCC_Demand_AOV = Field(
            name='AA SFCC Demand AOV',
            short_name='AA SFCC Dem AOV',
            sql_name='AA_SFCC_Demand_AOV',
            order=41,
            formatting=currency,
            subset=['fbb', 'revenue', 'rate', 'aa'],
            derived_columns=('AA SFCC Demand', 'AA Orders'),
            sql_mode='NULLABLE',
            field_type='FLOAT',
            cast_type='FLOAT64'
        )
        self.AA_Conversion = Field(
            name='AA Conversion',
            short_name='AA Conv',
            sql_name='AA_Conversion',
            order=42,
            formatting=Format.percent,
            subset=['aa', 'revenue', 'rate','aa_only'],
            derived_columns=('AA Orders', 'Clicks'),
            sql_mode='NULLABLE',
            field_type='FLOAT',
            cast_type='FLOAT64'
        )
        self.AA_Orders_per_AA_Visit = Field(
            name='AA Orders / AA Visit',
            short_name='AA Order / Visit',
            sql_name='AA_Orders_per_AA_Visit',
            order=44,
            formatting=Format.percent,
            subset=['fbb', 'revenue', 'rate', 'aa','aa_only'],
            derived_columns=('AA Orders', 'AA Visits'),
            sql_mode='NULLABLE',
            field_type='FLOAT',
            cast_type='FLOAT64',
            fbb_name='AA CVR'
        )
        self.AA_eCPM = Field(
            name='AA eCPM',
            short_name='AA eCPM',
            sql_name='AA_eCPM',
            order=46,
            formatting=currency,
            subset=['aa', 'revenue', 'rate','aa_only'],
            derived_columns=('AA Revenue', 'Delivered'),
            multiplier=1000,
            sql_mode='NULLABLE',
            field_type='FLOAT',
            cast_type='FLOAT64'
        )
        self.AA_SFCC_Demand_eCPM = Field(
            name='AA SFCC Demand eCPM',
            short_name='AA SFCC Dem eCPM',
            sql_name='AA_SFCC_Demand_eCPM',
            order=47,
            formatting=currency,
            subset=['fbb', 'revenue', 'rate', 'aa'],
            derived_columns=('AA SFCC Demand', 'Delivered'),
            multiplier=1000,
            sql_mode='NULLABLE',
            field_type='FLOAT',
            cast_type='FLOAT64',
            fbb_name='AA Demand $/M Email Sent'
        )
        self.Units = Field(
            name='Units',
            short_name='Units',
            sql_name='Units',
            order=47.05,
            formatting=Format.number,
            subset=['fbb', 'op'],
            sql_mode='NULLABLE',
            field_type='FLOAT',
            cast_type='FLOAT64',
        )
        self.Product_Margin = Field(
            name='Product Margin',
            short_name='Product Margin',
            sql_name='Product_Margin',
            order=47.1,
            formatting=currency,
            subset=['fbb', 'revenue', 'op'],
            sql_mode='NULLABLE',
            field_type='FLOAT',
            cast_type='FLOAT64',
            decription='The amount of SFCC Demand Revenue made on Margin, not accounting for Shipping.'
        )
        self.Product_Margin_Rate = Field(
            name='Product Margin %',
            short_name='Prod Margin %',
            sql_name='Product_Margin_Rate',
            order=47.2,
            formatting=Format.percent,
            subset=['fbb', 'revenue', 'rate'],
            derived_columns=('Product Margin', 'AA SFCC Demand'),
            sql_mode='NULLABLE',
            field_type='FLOAT',
            cast_type='FLOAT64',
            fbb_name='Product Margin %'
        )
        self.Total_Margin = Field(
            name='Total Margin',
            short_name='Total Margin',
            sql_name='Total_Margin',
            order=47.1,
            formatting=currency,
            subset=['fbb', 'revenue', 'op'],
            sql_mode='NULLABLE',
            field_type='FLOAT',
            cast_type='FLOAT64',
            decription='The amount of SFCC Demand Revenue made on Margin, accounting for Shipping.'
        )
        self.Total_Margin_Rate = Field(
            name='Total Margin %',
            short_name='Total Margin %',
            sql_name='Total_Margin_Rate',
            order=47.2,
            formatting=Format.percent,
            subset=['fbb', 'revenue', 'rate'],
            derived_columns=('Total Margin', 'AA SFCC Demand'),
            sql_mode='NULLABLE',
            field_type='FLOAT',
            cast_type='FLOAT64',
            fbb_name='Net Margin'
        )
        self.AA_SFCC_Demand_per_AA_Visit = Field(
            name='AA SFCC Demand / AA Visit',
            short_name='AA SFCC Dem / AA Visit',
            sql_name='AA_SFCC_Demand_per_AA_Visit',
            order=47.3,
            formatting=currency,
            subset=['aa', 'revenue', 'rate'],
            derived_columns=('AA SFCC Demand', 'AA Visits'),
            sql_mode='NULLABLE',
            field_type='FLOAT',
            cast_type='FLOAT64',
        )
        self.Unsub = Field(
            name='Unsub',
            short_name='Unsub',
            sql_name='Unsub',
            order=48,
            formatting=Format.number,
            subset=['nega_trends', 'churn', 'op', 'esp', 'engagement', 'fbb', 'all', 'esp_no_rev', 'sms'],
            sql_mode='NULLABLE',
            field_type='INTEGER',
            cast_type='FLOAT64'
        )
        self.Unsub_per_Delivered = Field(
            name='Unsub / Delivered',
            short_name='Unsub / Deliv',
            sql_name='Unsub_per_Delivered',
            order=49,
            formatting=Format.percent,
            subset=['nega_trends', 'churn', 'rate', 'esp', 'engagement', 'fbb', 'all', 'esp_no_rev', 'sms'],
            derived_columns=('Unsub', 'Delivered'),
            sql_mode='NULLABLE',
            field_type='FLOAT',
            cast_type='FLOAT64'
        )
        self.Complaints = Field(
            name='Complaints',
            short_name='Compl',
            sql_name='Complaints',
            order=50,
            formatting=Format.number,
            subset=['nega_trends', 'op'],
            sql_mode='NULLABLE',
            field_type='INTEGER',
            cast_type='FLOAT64'
        )
        self.Complaints_per_Delivered = Field(
            name='Complaints / Delivered',
            short_name='Compl / Deliv',
            sql_name='Complaints_per_Delivered',
            order=51,
            formatting=Format.percent,
            subset=['nega_trends', 'rate'],
            derived_columns=('Complaints', 'Delivered'),
            sql_mode='NULLABLE',
            field_type='FLOAT',
            cast_type='FLOAT64'
        )
        self.Hard_Bounces = Field(
            name='Hard Bounces',
            short_name='Hard Bounce',
            sql_name='Hard_Bounces',
            order=52,
            formatting=Format.number,
            subset=['nega_trends', 'op'],
            sql_mode='NULLABLE',
            field_type='INTEGER',
            cast_type='FLOAT64'
        )
        self.Soft_Bounces = Field(
            name='Soft Bounces',
            short_name='Soft Bounce',
            sql_name='Soft_Bounces',
            order=53,
            formatting=Format.number,
            subset=['nega_trends', 'op'],
            sql_mode='NULLABLE',
            field_type='INTEGER',
            cast_type='FLOAT64'
        )
        self.Total_Bounces = Field(
            name='Total Bounces',
            short_name='Total Bounce',
            sql_name='Total_Bounces',
            order=54,
            formatting=Format.number,
            subset=['nega_trends', 'op'],
            sql_mode='NULLABLE',
            field_type='INTEGER',
            cast_type='FLOAT64'
        )
        self.Total_Bounces_per_Delivered = Field(
            name='Total Bounces / Delivered',
            short_name='Total Bounce / Deliv',
            sql_name='Total_Bounces_per_Delivered',
            order=55,
            formatting=Format.percent,
            subset=['nega_trends', 'rate'],
            derived_columns=('Total Bounces', 'Delivered'),
            sql_mode='NULLABLE',
            field_type='FLOAT',
            cast_type='FLOAT64'
        )

    def __validate_name(self, name: str):
        if not re.match(r'^[A-Za-z_]+$', name[0]):
            raise ValueError(f'attr_name {name} must start with a letter or underscore.')
        return name

    def __validate_object_type(self, name: str, value: Field):
        if not isinstance(value, Field):
            raise ValueError(f'Parameter {name} must be an alx.Field object.')
        return value

    def __try_to_fill_all_names(self, value: str):
        if value.name:
            value.sql_name = value.sql_name or value.name.replace(' ', '_').replace('-', '_')
            value.short_name = value.short_name or value.name
        return value

    def __ensure_rates_are_made(self, name: str, value: Field):
        if 'rate' in value.subset:
            if value.derived_columns is None:
                raise ValueError(f"Error on {name}: this rate-column does not have a valid derivation.")
        return value

    def __sql_name_validation(self, value: Field):
        if not re.match(r'^\w+$', value.sql_name):
            raise ValueError(f'sql_name {value.sql_name} may only include [A-Za-z0-9_].')
        if not re.match(r'^[A-Za-z_]+$', value.sql_name[0]):
            raise ValueError(f'sql_name {value.sql_name} must start with a letter or underscore.')
        if re.match(r'^(?i)(_table_|_file_|_partition)', value.sql_name):
            raise ValueError(
                f'sql_name {value.sql_name} may not begin with case-insensitive '
                'special prefixes: _table_, _file_, _partition.'
            )
        return value

    def __setattr__(self, name: str, value: Field):
        name = self.__validate_name(name)
        value = self.__validate_object_type(name, value)
        value = self.__try_to_fill_all_names(value)
        value = self.__ensure_rates_are_made(name, value)
        value = self.__sql_name_validation(value)
        self.__dict__[name] = value

    def new_attribute(self, attr_name: str, value: Field = None, **kwargs):
        value = value or Field(**kwargs)
        return self.__setattr__(attr_name, value)

    def filter_part(self, field: str, part: str = 'name'):
        return field if part is None else field.dict().get(part)

    def subset(self, *subset: Tuple[str], part: str = None, sms: bool = False, ignore: List[str] = []):
        def subset_filter(subset_list):
            return list(map(
                self.filter_part,
                sorted(subset_list, key=lambda x: x.order),
                repeat(part)
            ))
        ignore = ignore + ['Opens', 'Opens / Delivered', 'Clicks / Open'] if sms else ignore
        if subset == ():
            return subset_filter([v for v in vars(self).values() if v.name not in ignore])
        else:
            return subset_filter([
                v for v in vars(self).values()
                if (
                    any([item for item in subset if item in v.subset])
                    and
                    v.name not in ignore
                )
            ])

    def search(self, search: str, default: str = None, part: str = None, errors: str = 'ignore') -> Field:
        """Search for Field object by header name.

        Arguments:
            search {str} -- The header name to search for.

        Returns:
            Field -- The Field object with the matching header name.

        TODO:
            - Craft Errors Enum
        """
        options = [
            field for field in vars(self).values()
            if search in [field.name, field.short_name, field.sql_name]
        ]
        try:
            return self.filter_part(options[0], part=part)
        except IndexError:
            if errors == 'raise':
                raise ValueError(f'Search key {search} did not return any matches.')
            elif errors == 'ignore':
                return default
            else:
                raise ValueError(f'Errors argument must be either "raise" or "ignore". You provided {errors}.')


heads = Heads()

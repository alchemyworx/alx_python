import datetime as dt

import pandas as pd


def fiscal_year(year: int, *, index: str = None) -> pd.DataFrame:
    """
    Determine FBB Fiscal Calendar dates with GRACE.

    Provided the year, build a Pandas DataFrame which contains dates along
    with their respective Fiscal Calendar Week, Month, Quarter, and Year.

    Arguments:
        year {int} -- the year of interest

    Returns:
        pd.DataFrame -- fiscal calendar data with fields for the
            fiscal week, month, quarter, and year
    """

    def fiscal_start(year: int, *, jan_limit: int = 4, dec_limit: int = 29) -> dt.datetime:
        """
        Determine the default start date of any given year.

        Default assumes that we are seeking a minimum of 4 January days in Week 1.
        We affirm this by checking the day-value of Sunday.

        Arguments:
            year {int} -- the year of interest

        Keyword Arguments:
            jan_limit {int} -- the maximum Jan day value which is allowed to lead the week.
                Logically never used... In theory, would keep us from using later weeks.
            dec_limit {int} -- the minumum Dec day value which is allowed to lead the week.
                Used as a measure to ensure the fiscal year is not being chosen too early.

        Returns:
            dt.datetime -- the Sunday object which leads the chosen fiscal year.
        """
        jan_1 = dt.datetime(year, 1, 1)
        start_date = (jan_1 - dt.timedelta(days=(jan_1.weekday() + 1)))
        if (start_date.month == 1) and (start_date.day > jan_limit):
            start_date -= dt.timedelta(weeks=1)
        elif (start_date.month == 12) and (start_date.day < dec_limit):
            start_date += dt.timedelta(weeks=1)
        return start_date

    date_range = pd.date_range(
        fiscal_start(year),
        fiscal_start(year + 1) - dt.timedelta(days=1),
    )
    weeks = [x for y in [[i] * 7 for i in range(1, (len(date_range) // 7) + 1)] for x in y]
    months = [x for y in [[m] * l * 7 for m, l in zip(range(1, 13), ([4, 4, 5] * 4))] for x in y]
    quarters = [x for y in [[i] * 13 * 7 for i in range(1, 5)] for x in y]
    if max(weeks) == 53:
        months += [12] * 7
        quarters += [4] * 7

    df_result = pd.DataFrame({
        'date': date_range,
        'week': weeks,
        'month': months,
        'quarter': quarters,
        'year': year
    })

    if index:
        df_result = df_result.set_index(index)

    return df_result


def extended_fiscal_calendar(start_year: int, end_year: int, *args, **kwargs) -> pd.DataFrame:
    """Appended length of Fiscal Calendar for the specified years.
    Calls fiscal_year() and passes over extra kwargs.

    Arguments:
        start_year {int} -- first year of interest
        end_year {int} -- last year of interest

    Returns:
        pd.DataFrame -- merged table with fiscal calendar details
    """

    return pd.concat([
        fiscal_year(year, *args, **kwargs)
        for year in range(start_year, end_year + 1)
    ])

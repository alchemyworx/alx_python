import alx.constants as const

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from enum import Enum
from typing import Union

from matplotlib import font_manager as fm

__all__ = [
    'format_axis',
    'plot_sheet'
]


class Axis(Enum):
    x: str = 'x'
    y: str = 'y'


def format_axis(ax, form: 'str', axis: Axis = 'y', limit_decimals: bool = True):
    """Format axis such that it is appropriate for the data the plot contains

    Arguments:
        ax {plt.Axes} -- ax plot to modify
        form {const.Format, str} -- format to modify the axis in the plot; either actual format string or reference to alx.format_dict

    Keyword Arguments:
        axis {Axis} -- ['x', 'y'] (default: {'y'})
        limit_decimals {bool} -- whether to restrict presence of decimals (default: {True})

    Raises:
        ValueError: 2D plots only

    Returns:
        None -- functions on axis in place
    """
    if hasattr(Axis, axis):
        axis = Axis(axis.lower())
        if axis == Axis.y:
            ticks = ax.get_yticks()

            def set_ticks(y):
                return ax.set_yticklabels(y)

        elif axis == Axis.x:
            ticks = ax.get_xticks()

            def set_ticks(x):
                return ax.set_xticklabels(x)

    else:
        raise ValueError("Only 2-D plots are supported. Use 'x' or 'y' axis.")

    if form in const.column_format:
        if form in ['percent', const.format_dict['percent']]:
            ticklabels = [const.column_format[form].format(x) for x in ticks]
        else:
            ticklabels = [const.column_format.get(form, const.column_format.get('number')).format(x if 1 >= x >= -1 else int(x)) for x in ticks]
    else:
        try:
            ticklabels = [form.format(x) for x in ticks]
        except:
            ticklabels = [const.format_dict['number'].format(x) for x in ticks]
    set_ticks(ticklabels)


def plot_sheet(
    df: pd.DataFrame,
    defaults: bool = True,
    specifics: dict = {},
    nrows: int = 0,
    ncols: int = 0,
    figsize: dict = (),
    wspace: float = 0.2,
    hspace: float = 0.35,
    title_prop=fm.FontProperties(),
    legend_prop=fm.FontProperties(),
    heads: const.heads = const.heads,
    xrotate: Union[float, int] = None,
    **kwargs
) -> plt.figure:
    """Provided a hierarchical dataframe, a series of comparative plots are stitched into a single image.

    Titles are copied from the column header unless otherwise specified via specifics or **kwargs.
    Plots are placed in the order which they are specified in the df.

    kwargs are passed through to pyplot.

    Arguments:
        df {pd.DataFrame} -- dataframe, typically with conventional ALX headers

    Keyword Arguments:
        defaults {bool} -- whether to apply default layout settings, including: (default: {True})
            default_kwargs = {
                'title': col,
                'grid': True,
                'legend': False,
                'linewidth': 2
            }
        specifics {dict} -- user specifics to apply to the overall figure. Overrides default settings (default: {{}})
        nrows {int} -- number of rows in figure grid (default: {0})
        ncols {int} -- number of cols in figure grid (default: {0})
        figsize {dict} -- figsize (default: {()})
        wspace {float} -- vertical space between plots (default: {0.2})
        hspace {float} -- horizontal space between plots (default: {0.35})

    Raises:
        ValueError: This function only has preset-layout instructions for up to 6 plots. Specify your own for more.

    Returns:
        plt.figure -- the figure containing all applicable plots
    """

    top_most_cols = df.columns.get_level_values(0).unique()

    if any([nrows, ncols]):
        if all([nrows, ncols]):
            pass
        elif ncols:
            nrows = int(np.ceil(ncols / len(top_most_cols)))
        elif nrows:
            ncols = int(np.ceil(nrows / len(top_most_cols)))
    else:
        if len(top_most_cols) < 4:
            nrows = 1
        elif len(top_most_cols) < 7:
            nrows = 2
        elif len(top_most_cols) < 13:
            nrows = 3
        else:
            raise ValueError('Limit table top-level headers to 12 plots or define your own layout using nrows, ncols.')
        ncols = int(np.ceil(len(top_most_cols) / nrows))
    figsize = (8 * ncols, 4 * nrows) if not figsize else figsize

    # Initialize plot_sheet
    fig, axes = plt.subplots(nrows, ncols, figsize=figsize)
    plt.subplots_adjust(wspace=wspace, hspace=hspace)
    x, y = 0, 0

    # Plot column data.
    for i, col in enumerate(top_most_cols):
        if (nrows * ncols) == 1:
            ax = axes
        else:
            ax = axes[y, x] if nrows > 1 else axes[x]

        # Remove delicate kwargs from dicts which are passed
        # to external, dependent functions.
        tick_format = specifics[col].pop('tick_format', None) if col in specifics else None
        if tick_format is None:
            tick_format = heads.search(col, part='formatting')
        if tick_format is None:
            tick_format = 'number'

        # Define Default settings.
        default_kwargs = {
            'title': col,
            'grid': True,
            'legend': False,
            'linewidth': 2
        }

        if defaults:
            approved_kwargs = {
                **kwargs,
                **{k: v for k, v in default_kwargs.items() if k not in kwargs}
            }
        else:
            approved_kwargs = kwargs

        # Handle specific instructions.
        formatted = False
        if col in specifics:
            if defaults:
                approved_specs = {
                    **specifics[col],
                    **{k: v for k, v in default_kwargs.items() if k not in specifics[col]}
                }
            else:
                approved_specs = specifics[col]
            ax = approved_specs.pop('ax', ax)
            secondary_y = approved_specs.pop('secondary_y', None)
            if secondary_y is not None:  # TODO: Secondary Y Axis is handled, however, the X axis disappears and is unrecoverable?
                color = approved_specs.pop('color', None)
                ax1 = df[col][[c for c in df[col].columns if c not in secondary_y]].plot(ax=ax, color=color[:len([c for c in df[col].columns if c not in secondary_y])], **approved_specs)
                ax2 = df[col][[c for c in df[col].columns if c in secondary_y]].plot(ax=ax, secondary_y=True, color=color[len([c for c in df[col].columns if c in secondary_y]):], **approved_specs)

                from matplotlib import ticker
                lim1 = ax1.get_ylim()
                lim2 = ax2.get_ylim()
                f = lambda x: lim2[0] + (x - lim1[0]) / (lim1[1] - lim1[0]) * (lim2[1] - lim2[0])
                ticks = f(ax1.get_yticks())
                ax2.yaxis.set_major_locator(ticker.FixedLocator(ticks))

                if i == 0:
                    h1, l1 = ax1.get_legend_handles_labels()
                    h2, l2 = ax2.get_legend_handles_labels()
                    l2 = [f'{l} [R]' for l in l2]
                    handles = h1 + h2
                    labels = l1 + l2

                if defaults:
                    axis = 'x' if kwargs.get('kind', False) == 'barh' else 'y'
                    try:
                        format_axis(ax1, tick_format, axis=axis)
                        ax2.set_yticklabels([
                            const.column_format.get(
                                tick_format,
                                tick_format
                            ).format(
                                round(x, -int(len(str(int(x))) / 2))
                            ) for x in ax2.get_yticks()
                        ])
                    except IndexError:
                        format_axis(ax1, 'number', axis=axis)
                        format_axis(ax2, 'number', axis=axis)
                    finally:
                        formatted = True

            else:
                ax = df[col].plot(ax=ax, **approved_specs)
                if i == 0:
                    handles, labels = ax.get_legend_handles_labels()
        else:
            ax = df[col].plot(ax=ax, **approved_kwargs)
            if i == 0:
                handles, labels = ax.get_legend_handles_labels()

        if defaults:
            if title_prop:
                ax.set_title(
                    ax.get_title(),
                    fontproperties=title_prop,
                    fontsize=18
                )
            else:
                ax.set_title(
                    ax.get_title(),
                    fontsize=18
                )
            if not formatted:
                axis = 'x' if kwargs.get('kind', False) == 'barh' else 'y'
                try:
                    format_axis(ax, tick_format, axis=axis)
                except IndexError:
                    format_axis(ax, 'number', axis=axis)
                finally:
                    formatted = True
        if xrotate:
            ax.tick_params(axis='x', labelrotation=xrotate)

        # Handle the coordinate locators for the next plot.
        if x < ncols - 1:
            x += 1
        else:
            x = 0
            y += 1

    if defaults:
        if kwargs.get('legend', True) & all([handles, labels]):
            if legend_prop:
                fig.legend(handles, labels, loc='lower center', ncol=len(labels), prop=legend_prop)
            else:
                fig.legend(handles, labels, loc='lower center', ncol=len(labels))

    # If there are too many subplots, delete the extras.
    extra_cells = ((nrows * ncols) - (i + 1))
    if extra_cells:
        x = ncols - 1
        y = nrows - 1

        for _ in range(extra_cells):
            ax = axes[y, x]
            ax.set_axis_off()

            # Handle the coordinate locators for the next plot.
            if x > 0:
                x -= 1
            else:
                x = ncols - 1
                y -= 1
    return fig

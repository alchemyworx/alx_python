
__all__ = [
    'sql_str_fix'
]


def sql_str_fix(s: str, inv: bool = False) -> str:
    """Change the expected alx.headers to an appropriate SQL form which has no spaces or symbols.

    Arguments:
        s {str} -- the string to be modified

    Keyword Arguments:
        inv {bool} -- whether to inverse the process (default: {False})

    Returns:
        str -- string modified for SQL (or reversed)
    """
    # TODO: Standardize formatting
    # - includes unique flag within underscore delimiter to ensure there are no false transformations.
    # - This will require all data to be completely overwritten.

    for sett in [
        ('\t', '_tab_'),
        (':', '_colon_'),
        (';', '_semicolon_'),
        ('%', '_percent_'),
        ('&', '_ampersand_'),
        ('$', '_dollar_'),
        ('!', '_exclamation_'),
        ('?', '_question_'),
        ('(', '_parenthesis_'),
        (')', '_endparenthesis_'),
        ('+/-', '_plusminus_'),
        ('+', '_plus_'),
        ('-', '_dash_'),
        (' / ', '_per_'),
        ('/', '_slash_'),
        ('.', '_dot_'),
        (' ', '_')
    ]:
        original, new = (sett[1], sett[0]) if inv else (sett[0], sett[1])
        s = s.replace(original, new)
    return s

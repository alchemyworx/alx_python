import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="alx",
    version="0.1.3",
    author="Walter Zielenski",
    author_email="tech@alchemyworx.com",
    description="A Python Library for Alchemy Worx internal, standardized use.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3.7",
        "Operating System :: Linux",
    ],
    include_package_data=True,
    python_requires='>=3.6.8',
    install_requires=[
        'pandas',
        'matplotlib',
        'numpy',
        "python-dateutil",
        'pydantic'
    ],
)
